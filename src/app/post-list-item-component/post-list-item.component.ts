import {Component, Input, OnInit} from '@angular/core';
import {Post} from '../models/Post';
import {Router} from '@angular/router';
import {PostService} from '../services/post.service';


@Component({
  selector: 'app-post-list-item-component',
  templateUrl: './post-list-item.component.html',
  styleUrls: ['./post-list-item.component.css']
})

export class PostListItemComponent implements OnInit {
  @Input() post: Post;

  constructor(
    private postService: PostService,
    private router: Router) { }

  ngOnInit() {
  }

  like() {
      this.post.loveIts++;
  }
  disLike() {
      this.post.loveIts--;
  }
  onGetOne(id: number) {
    this.router.navigate(['posts', id]);
  }
  onRemove () {
    if(this.post){
      this.postService.remove(this.post);
    }
  }

}
