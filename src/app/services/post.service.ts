import { Injectable } from '@angular/core';
import {Post} from '../models/Post';
import {Subject} from 'rxjs';
import * as firebase from 'firebase';

@Injectable({
  providedIn: 'root'
})
export class PostService {
  posts: Post[] = [];
  postsSubject: Subject<Post[]> = new Subject<Post[]>();

  constructor() {
    this.getAll();
  }
  emitSubject() {
    this.postsSubject.next(this.posts);
  }

  save() {
    firebase.database().ref('posts').set(this.posts);
  }
  getAll() {
    firebase.database().ref('/posts').on('value', (data) => {
      this.posts = data.val() ? data.val() : [];
      this.emitSubject();
    });
  }
  get(id: number): Promise<Post> {
    return new Promise<Post>(((resolve, reject) => {
      firebase.database().ref('posts/' + id).once('value',
         (data) => {resolve(data.val()); },
         (e) => {console.log(e); });
    }));
  }
  create(post: Post) {
    post.id = this.posts.length;
    this.posts.push(post);
    this.save();
  }
  remove(post: Post): void {
    const i = this.posts.findIndex((item) => {
      return item === post;
    });
    this.posts.splice(i, 1);
    this.save();
  }
}
