export class Post {
  public id: number;
  public created_at: string;
  public loveIts: number;

  constructor(public title: string, public content: string) {
    this.created_at = new Date().toString();
    this.loveIts = 0;
  }
}
