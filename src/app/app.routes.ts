import {Route} from '@angular/router';
import {PostListComponent} from './post-list-component/post-list.component';
import {PostFormComponent} from './post-form/post-form.component';
import {FileNotFoundComponent} from './file-not-found/file-not-found.component';
import {PostOneComponent} from './post-one/post-one.component';

export const appRoutes: Route[] = [
  {path: 'posts', component: PostListComponent},
  {path: 'posts/new', component: PostFormComponent},
  {path: 'posts/:id', component: PostOneComponent},
  {path: '404', component: FileNotFoundComponent},
  {path: '', redirectTo: '/posts', pathMatch: 'full'},
  {path: '**', redirectTo: '404'}
];
