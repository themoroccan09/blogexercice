import {Component} from '@angular/core';
import * as firebase from 'firebase';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  constructor() {
    this.initFirebase();
  }

  initFirebase() {
    // Initialize Firebase
    const config = {
      apiKey: 'AIzaSyBtW3-8VhHNs8Z1YRphbqN9b1Fst_UB6Zo',
      authDomain: 'test-23254.firebaseapp.com',
      databaseURL: 'https://test-23254.firebaseio.com',
      projectId: 'test-23254',
      storageBucket: 'test-23254.appspot.com',
      messagingSenderId: '524584100179'
    };
    firebase.initializeApp(config);
  }
}
